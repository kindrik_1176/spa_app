var gulp = require('gulp'),
    gutil = require('gulp-util');
    
    
var sassSource = ['sass/master.scss'];

gulp.task('sass', function(){
    gulp.src(sassSource)
        .on('error', gutil.log)
        .pipe(gulp.dest('css'));
});