/*global angular*/

var app = angular.module("spa_app", ["ngRoute"]);


app.config(['$routeProvider', function($routeProvider){
    
    $routeProvider
    
    .when('/', {
        templateUrl:"main_content.html"
    })
    
    .when('/about', {templateUrl:'about.html'})
    .when('/portfolio', {templateUrl:'portfolio.html'})
    .when('/skills', {templateUrl:'skills.html'})
    .when('/contact', {templateUrl:'contact.html'});
    
}]);

app.controller("spa_ctrl", ["$scope", function($scope){
    $scope.header_content = 'Josh Winters Code Examples';
}]);